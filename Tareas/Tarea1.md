# **_La Vida..._** 

>La vida es una oportunidad,
aprovéchala, la vida es belleza, admírala,
la vida es beatitud, saboréala, 
la vida es un sueño, hazlo realidad.
La vida es un reto, afróntalo;
la vida es un juego, juégalo, la vida es preciosa,
cuídala; la vida es riqueza, consérvala;
la vida es un misterio, descúbrelo.
La vida es una promesa, cúmplela;
la vida es amor, gózalo; la vida es tristeza,
supérala; la vida es un himno, cántalo;
la vida es una tragedia, dominala.
La vida es aventura, vívela;
la vida es felicidad, merécela;
la vida es vida, defiéndela... 

  
**Autor:** _María Teresa de Calcuta._